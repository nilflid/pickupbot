# -*- coding: utf-8 -*-

import telebot # Librería de la API del bot.
from telebot import types # Tipos para la API del bot.
import time # Librería para hacer que el programa que controla el bot no se acabe.
from meme_generator import generateNewBot
from scrapper import scrapPage


TOKEN = '139123001:AAGnvCmahqcicwICkVcgT5Z6rerYXk94hQY' # Nuestro tokken del bot (el que @BotFather nos dió).

bot = telebot.TeleBot(TOKEN) # Creamos el objeto de nuestro bot.
#############################################
#Listener
def listener(messages): 
    for m in messages:
        cid = m.chat.id 
        #print "[" + str(cid) + "]: " + m.text
bot.set_update_listener(listener) # Así, le decimos al bot que utilice como función escuchadora nuestra función 'listener' declarada arriba.
#############################################
#Funciones
@bot.message_handler(commands=['line']) # Indicamos que lo siguiente va a controlar el comando '/miramacho'
def command_pickupline_line(m): # Definimos una función que resuleva lo que necesitemos.
    cid = m.chat.id # Guardamos el ID de la conversación para poder responder.
    scarppedMessage = scrapPage()
    bot.send_message( cid, scarppedMessage) # Con la función 'send_message()' del bot, enviamos al ID almacenado el texto que queremos.bot
    
@bot.message_handler(commands=['image']) # Indicamos que lo siguiente va a controlar el comando '/miramacho'
def command_pickupline_image(m): # Definimos una función que resuleva lo que necesitemos.
    cid = m.chat.id
    messageToParse = m.text.replace('/image', '')
    messages = (messageToParse).split('_')
    messageUp = messageToParse
    messageDown = ''
    if(messages.__len__() > 0):
        messageUp = messages[0]

    if(messages.__len__() > 1):
        messageDown = messages[1]
        
    url = generateNewBot(messageUp, messageDown)
    print('The url is: ' + url)
    bot.send_photo( cid, url) # Con la función 'send_photo()' del bot, enviamos al ID de la conversación que hemos almacenado previamente la foto de nuestro querido :roto2:#############################################
#Peticiones
bot.polling(none_stop=True) # Con esto, le decimos al bot que siga funcionando incluso si encuentra algún fallo.