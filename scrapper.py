import requests
import random
from bs4 import BeautifulSoup

def scrapPage():
    pageNumber = random.randrange(2, 68)
    response = requests.get('http://pickup-lines.net/page/' + str(pageNumber))
    rawMessage = response.text
    soup = BeautifulSoup(rawMessage, 'lxml')
    spans = soup.find_all('span', { "class" : "loop-entry-line" })
    parsedSpans =  [ singleSpan.text for singleSpan in spans ]
    numberOfElements = len(parsedSpans)
    indexOfElementToChoose = random.randrange(0, numberOfElements)
    choosenElementHTML = parsedSpans[indexOfElementToChoose]
    return choosenElementHTML
